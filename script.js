const countToDate = new Date().setHours(19, 0, 0);
let previousTimeBetweenDates;
setInterval(() => {
  const currentDate = new Date();
  const timeBetweenDates = Math.ceil((countToDate - currentDate) / 1000);
  flipAllCards(timeBetweenDates);

  previousTimeBetweenDates = timeBetweenDates;
}, 250);

function flipAllCards(time) {
  const seconds = time % 60;
  const minutes = Math.floor(time / 60) % 60;
  const hours = Math.floor(time / 3600);

  let second = document.querySelectorAll("[data-seconds-ones]");
  let mil = document.querySelectorAll("[data-seconds-tens]");
  let hourTen = document.querySelectorAll("[data-hours-tens]");
  let hourOne = document.querySelectorAll("[data-hours-ones]");
  let minTen = document.querySelectorAll("[data-minutes-tens]");
  let minOne = document.querySelectorAll("[data-minutes-ones]");

  for (let i = 0; i < hourTen.length; i++) {
    flip(hourTen[i], Math.floor(hours / 10));
  }
  for (let i = 0; i < hourOne.length; i++) {
    flip(hourOne[i], hours % 10);
  }
  for (let i = 0; i < minTen.length; i++) {
    flip(minTen[i], Math.floor(minutes / 10));
  }
  for (let i = 0; i < minOne.length; i++) {
    flip(minOne[i], minutes % 10);
  }
  for (let i = 0; i < mil.length; i++) {
    flip(mil[i], Math.floor(seconds / 10));
  }
  for (let i = 0; i < second.length; i++) {
    flip(second[i], seconds % 10);
  }
}

function flip(flipCard, newNumber) {
  const topHalf = flipCard.querySelectorAll(".top")[0];
  const startNumber = parseInt(topHalf.textContent);
  if (newNumber === startNumber) return;

  const bottomHalf = flipCard.querySelectorAll(".bottom")[0];
  const topFlip = document.createElement("div");
  topFlip.classList.add("top-flip");
  const bottomFlip = document.createElement("div");
  bottomFlip.classList.add("bottom-flip");

  top.textContent = startNumber;
  bottomHalf.textContent = startNumber;
  topFlip.textContent = startNumber;
  bottomFlip.textContent = newNumber;

  topFlip.addEventListener("animationstart", (e) => {
    topHalf.textContent = newNumber;
  });
  topFlip.addEventListener("animationend", (e) => {
    topFlip.remove();
  });
  bottomFlip.addEventListener("animationend", (e) => {
    bottomHalf.textContent = newNumber;
    bottomFlip.remove();
  });
  flipCard.append(topFlip, bottomFlip);
}
